// Create namespace
if (!window.com) com = new Object();
if (!com.roccoor) com.roccoor = new Object();
if (!com.roccoor.bmi) com.roccoor.bmi = new Object();

com.roccoor.bmi.evaluateBmi = function() {

	var bmi = new Number(window.bmiData.bmi);
	bmi = formatDecimal(window.bmiData.bmi, 2);
	var message;
	var image;
	
	//Underweight = <18.5
	//Normal weight = 18.5�24.9
	//Overweight = 25�29.9
	//Obesity = BMI of 30 or greater
	if (window.bmiData.bmi <= 0) {
		message = "You've just messed it up!<br/>Your data input is not computable!";
		image = "images/misc/Cute-Ball-Stop-icon.png";
	}
	else if (window.bmiData.bmi < 18.5) {
		message = "You are the skinny type!<br/> Eat more!";
		image = "images/fatman/FatMan-1.png";
	}
	else if (window.bmiData.bmi < 24.9) {
		message = "You eat just right!";
		image = "images/fatman/FatMan-2.png";
	}
	else if (window.bmiData.bmi < 29.9) {
		message = "You eat more than you need!<br/> Do more sports!";
		image = "images/fatman/FatMan-3.png";
	}
	else {
		message = "You are an eating machine!<br/> Stop eating! Start dieting!"; 
		image = "images/fatman/FatMan-4.png";
	}
	
	document.getElementById("message").innerHTML = message;
	document.getElementById("fatman").src = image;
}

com.roccoor.bmi.exit = function() {
	blackberry.app.exit();
}

// Helper functions
function formatDecimal(num, numPlaces){
			var snum = new String(parseFloat(num))
			var i=z=0; 
			var sout=ch="" 
			while(i<snum.length && ch != '.' ){
			ch = snum.charAt(i++)
			sout+=ch
			}
			while(i<snum.length && z++<numPlaces){
				ch = snum.charAt(i++)
				sout+=ch
			}
			if(snum.indexOf('.')==-1)sout+='.';
			while(z++<numPlaces)sout+='0';
			return sout
}


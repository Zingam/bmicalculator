// Create namespace
if (!window.com) com = new Object();
if (!com.roccoor) com.roccoor = new Object();
if (!com.roccoor.bmi) com.roccoor.bmi = new Object();

// Create global bmiData Object
if (!window.bmiData) window.bmiData = new Object();

com.roccoor.bmi.initialize_screenBmi = function() {
	window.bmiData.units = 'imperial';
}

// pill-buttons functions BEGIN
com.roccoor.bmi.pillButton_selectImperial = function() {
	document.getElementById('panel_Imperial').style.display = 'inline';
	document.getElementById('panel_Metric').style.display = 'none';

	window.bmiData.units = 'imperial';
}

com.roccoor.bmi.pillButton_selectMetric = function() {
	document.getElementById('panel_Imperial').style.display = 'none';
	document.getElementById('panel_Metric').style.display = 'inline';
	
	window.bmiData.units = 'metric';
}
// pill-buttons functions END

com.roccoor.bmi.getData_Imperial = function() {
	// Collect user input
	var height_ft = parseFloat(document.getElementById('inputNumber_Height_ft').value);
	var height_in = parseFloat(document.getElementById('inputNumber_Height_in').value);
	var weight_lb = parseFloat(document.getElementById('inputNumber_Weight_lb').value);
	
	// Fix user input
	if (isNaN(height_ft) || height_ft < 0)
		height_ft = 0;
	if (isNaN(height_in) || height_in < 0)
		height_in = 0;
	if (isNaN(weight_lb) || weight_lb < 0)
		weight_lb = 0;
	
	var totalHeight_in = 12 * height_ft + height_in;
	
	// Convert and save the collected data
	window.bmiData.height = totalHeight_in / 0.393701; // 1 cm = 0.393701 in;
	window.bmiData.weight = weight_lb / 2.2046226218; // 1 kg = 2.2046226218 lb
}

com.roccoor.bmi.getData_Metric = function() {
	// Collect user input
	var height_m = parseFloat(document.getElementById('inputNumber_Height_m').value);
	var height_cm = parseFloat(document.getElementById('inputNumber_Height_cm').value);
	var weight_kg = parseFloat(document.getElementById('inputNumber_Weight_kg').value);
	
	// Fix user input
	if (isNaN(height_m) || height_m < 0)
		height_m = 0;
	if (isNaN(height_cm) || height_cm < 0)
		height_cm = 0;
	if (isNaN(weight_kg) || weight_kg < 0)
		weight_kg = 0;
		
	var totalHeight_cm = 100 * height_m + height_cm;
	
	// Save the collected data
	window.bmiData.height = totalHeight_cm;
	window.bmiData.weight = weight_kg;
}
	
com.roccoor.bmi.calculateBmi = function() {
		'use strict';
		
		var message;
		if (window.bmiData.units === 'imperial')
			com.roccoor.bmi.getData_Imperial();
		else
			com.roccoor.bmi.getData_Metric();

		// Use metric formula to calculate BMI since all units are already converted
		var bmi = window.bmiData.weight / (window.bmiData.height * window.bmiData.height) * 10000;
		if (isNaN(bmi) || !isFinite(bmi))
			bmi = 0;
		window.bmiData.bmi = bmi;
}

com.roccoor.bmi.displayBmi = function() {
	com.roccoor.bmi.calculateBmi();
	bb.pushScreen("screenResult.html", "screenResult");
}

// Helper functions
com.roccoor.bmi.log10 = function(value) {
	return Math.log(value) / Math.LN10;
}